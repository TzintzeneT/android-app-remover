import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {

    public static Scanner in = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        String cofigAppsToRemove;
        try {
            cofigAppsToRemove = "packages.conf";
        }
        catch (Exception error){
            System.out.println("no config file :(");
            cofigAppsToRemove = "";
        }

        System.out.println("Starting...");

        System.out.println("\nthe following packages (apps) will be removed: ");
        String appList = Files.readString(Paths.get(cofigAppsToRemove));
        System.out.println(appList + "\n");

        System.out.print("press Enter to continue...");
        in.nextLine();


        for (String app: appList.split(",")){
            String [] uninstallCommand = new String[] {"adb", "uninstall", app.replace(" ", "")};
            System.out.println("removing " + uninstallCommand[2]);
            Runtime.getRuntime().exec(uninstallCommand, null, null);
        }
    }
}
